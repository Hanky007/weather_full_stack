from django.db import models

class weather_db(models.Model):
	city_id=models.CharField(max_length=100);
	day=models.CharField(max_length=20);
	temp_min=models.CharField(max_length=5);
	temp_max=models.CharField(max_length=5);
	rainfall=models.CharField(max_length=5);
	
	def __str__(self):
		return self.city_id + ", " + self.day + ", " + self.temp_max + ", "+ self.rainfall;

# # Create your models here.
