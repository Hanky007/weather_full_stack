from django.conf.urls import include
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name="this is the proj"),
    url(r'^delete/$', views.delete_db, name="clear the entire db")
    # url(r'^all$', views.print_db, name=""),
    # url(r'^..*$', views.err_msg, name="ERROR MSG HERE")
]
